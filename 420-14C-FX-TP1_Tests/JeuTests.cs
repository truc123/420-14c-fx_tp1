#region M�TADONN�ES

// Nom du fichier : JeuTests.cs
// Auteur : M�lina Hotte (1933760)
// Date de cr�ation : 2021-02-26
// Date de modification : 2021-02-26

#endregion

#region USING

using _420_14C_FX_TP1.Classes;
using System;
using System.Collections.Generic;
using System.Drawing;
using Xunit;

#endregion

namespace _420_14C_FX_TP1_Test
{
    public class JeuTests
    {
        [Fact]
        public void Joueur_Devrait_Etre_Defini_Quand_Valeur_Est_Non_Vide()
        {
            // Arrange
            Jeu jeu = new Jeu(new Joueur("Test"));

            // Act et assert
            Assert.Throws<ArgumentNullException>(() => jeu.Joueur = null);
        }

        [Fact]
        public void LstElementsGraphiques_Devrait_Etre_Defini_Quand_Valeur_Est_Non_Vide()
        {
            // Arrange
            Jeu jeu = new Jeu(new Joueur("Test"));

            // Act et assert
            Assert.Throws<ArgumentNullException>(() => jeu.LstElementsGraphiques = null);
        }

        [Fact]
        public void Constructeur_Devrait_Creer_Instance_Quand_Joueur_Est_Non_Nul()
        {
            // Arrange, act et assert
            Assert.Throws<ArgumentNullException>(() => new Jeu(new Joueur(null)));
        }

        [Fact]
        public void DemarrerPartie_Devrait_Ajouter_Tous_Elements_A_Afficher_Dans_Liste()
        {
            // Arrange
            Jeu jeu = new Jeu(new Joueur("Test"));

            // Act
            jeu.DemarrerPartie();

            // Assert
            Assert.Equal(6, jeu.LstElementsGraphiques.Count);

            foreach (ElementGraphique elementGraphique in jeu.LstElementsGraphiques)
                Assert.NotNull(elementGraphique);
        }

        [Fact]
        public void ValiderCollisions_Devrait_Retirer_ElementGraphique_Detruit_De_Liste_Quand_Heurter_VaisseauJoueur()
        {
            // Permet de tester toutes les collisions possibles pour le vaisseau du joueur

            // Arrange
            List<int> lstNbElementsGraphiquesApresCollision = new List<int>();

            List<ElementGraphique> lstElementsGraphiquesAHeurter = new List<ElementGraphique>
            {
                new Asteroide(Jeu.LARGEUR_ECRAN / 2, Jeu.HAUTEUR_ECRAN - 50),
                new CapsuleEnergie(Jeu.LARGEUR_ECRAN / 2, Jeu.HAUTEUR_ECRAN - 50),
                new Missile(Jeu.LARGEUR_ECRAN / 2, Jeu.HAUTEUR_ECRAN - 50, Direction.Bas, 2),
                new Laser(Jeu.LARGEUR_ECRAN / 2, Jeu.HAUTEUR_ECRAN - 50),
                new Ennemi(Jeu.LARGEUR_ECRAN / 2, Jeu.HAUTEUR_ECRAN - 50)
            };

            for (int i = 0; i < lstElementsGraphiquesAHeurter.Count; i++)
            {
                Joueur joueur = new Joueur("Test");
                Jeu jeu = new Jeu(joueur);

                jeu.LstElementsGraphiques.Clear();
                jeu.LstElementsGraphiques.Add(joueur.Vaisseau);
                jeu.LstElementsGraphiques.Add(lstElementsGraphiquesAHeurter[i]);
                jeu.LstElementsGraphiques.Add(new Boss(Jeu.LARGEUR_ECRAN / 2 - 38, 0));

                // Act
                jeu.ValiderCollisions();
                lstNbElementsGraphiquesApresCollision.Add(jeu.LstElementsGraphiques.Count);
            }

            // Assert
            Assert.Equal(new List<int> { 0, 2, 2, 2, 2 }, lstNbElementsGraphiquesApresCollision);
        }

        [Fact]
        public void ValiderCollisions_Devrait_Retirer_ElementGraphique_Detruit_De_Liste_Quand_Heurter_Missile()
        {
            // Permet de tester toutes les combinaisons de collisions possibles non tester pr�c�demment pour un missile
            // Arrange
            List<int> lstNbElementsGraphiquesApresCollision = new List<int>();

            List<ElementGraphique> lstElementsGraphiquesAHeurter = new List<ElementGraphique>
            {
                new Asteroide(Jeu.LARGEUR_ECRAN / 2, 200),
                new CapsuleEnergie(Jeu.LARGEUR_ECRAN / 2, 200),
                new Missile(Jeu.LARGEUR_ECRAN / 2, 200, Direction.Bas, 2),
                new Laser(Jeu.LARGEUR_ECRAN / 2, 200),
                new Ennemi(Jeu.LARGEUR_ECRAN / 2, 200)
            };

            for (int i = 0; i < lstElementsGraphiquesAHeurter.Count; i++)
            {
                Joueur joueur1 = new Joueur("Test");
                Jeu jeu1 = new Jeu(joueur1);

                jeu1.LstElementsGraphiques.Clear();
                jeu1.LstElementsGraphiques.Add(joueur1.Vaisseau);
                jeu1.LstElementsGraphiques.Add(lstElementsGraphiquesAHeurter[i]);
                jeu1.LstElementsGraphiques.Add(new Boss(Jeu.LARGEUR_ECRAN / 2 - 38, 0));
                jeu1.LstElementsGraphiques.Add(new Missile(Jeu.LARGEUR_ECRAN / 2, 200, Direction.Haut, 2));

                // Act
                jeu1.ValiderCollisions();
                lstNbElementsGraphiquesApresCollision.Add(jeu1.LstElementsGraphiques.Count);
            }

            // Cas missile heurte le boss
            Joueur joueur2 = new Joueur("Test");
            Jeu jeu2 = new Jeu(joueur2);

            jeu2.LstElementsGraphiques.Clear();
            jeu2.LstElementsGraphiques.Add(joueur2.Vaisseau);
            jeu2.LstElementsGraphiques.Add(new Boss(Jeu.LARGEUR_ECRAN / 2, 0));
            jeu2.LstElementsGraphiques.Add(new Missile(Jeu.LARGEUR_ECRAN / 2, 0, Direction.Haut, 2));

            // Act
            jeu2.ValiderCollisions();
            lstNbElementsGraphiquesApresCollision.Add(jeu2.LstElementsGraphiques.Count);

            // Assert
            Assert.Equal(new List<int> { 2, 2, 2, 2, 2, 2 }, lstNbElementsGraphiquesApresCollision);
        }

        [Fact]
        public void ValiderCollisions_Devrait_Retirer_ElementGraphique_Detruit_De_Liste_Quand_Heurter_Laser()
        {
            // Permet de tester toutes les combinaisons de collisions possibles non tester pr�c�demment pour un laser
            // Arrange
            List<int> lstNbElementsGraphiquesApresCollision = new List<int>();

            List<ElementGraphique> lstElementsGraphiquesAHeurter = new List<ElementGraphique>
            {
                new Asteroide(Jeu.LARGEUR_ECRAN / 2, 200),
                new CapsuleEnergie(Jeu.LARGEUR_ECRAN / 2, 200),
                new Laser(Jeu.LARGEUR_ECRAN / 2, 200),
                new Ennemi(Jeu.LARGEUR_ECRAN / 2, 200)
            };

            for (int i = 0; i < lstElementsGraphiquesAHeurter.Count; i++)
            {
                Joueur joueur1 = new Joueur("Test");
                Jeu jeu1 = new Jeu(joueur1);

                jeu1.LstElementsGraphiques.Clear();
                jeu1.LstElementsGraphiques.Add(joueur1.Vaisseau);
                jeu1.LstElementsGraphiques.Add(lstElementsGraphiquesAHeurter[i]);
                jeu1.LstElementsGraphiques.Add(new Boss(Jeu.LARGEUR_ECRAN / 2 - 38, 0));
                jeu1.LstElementsGraphiques.Add(new Laser(Jeu.LARGEUR_ECRAN / 2, 200));

                // Act
                jeu1.ValiderCollisions();
                lstNbElementsGraphiquesApresCollision.Add(jeu1.LstElementsGraphiques.Count);
            }

            // Cas laser heurte le boss
            Joueur joueur2 = new Joueur("Test");
            Jeu jeu2 = new Jeu(joueur2);

            jeu2.LstElementsGraphiques.Clear();
            jeu2.LstElementsGraphiques.Add(joueur2.Vaisseau);
            jeu2.LstElementsGraphiques.Add(new Boss(Jeu.LARGEUR_ECRAN / 2, 0));
            jeu2.LstElementsGraphiques.Add(new Laser(Jeu.LARGEUR_ECRAN / 2, 0));

            // Act
            jeu2.ValiderCollisions();
            lstNbElementsGraphiquesApresCollision.Add(jeu2.LstElementsGraphiques.Count);

            // Assert
            Assert.Equal(new List<int> { 2, 2, 2, 2, 2 }, lstNbElementsGraphiquesApresCollision);
        }

        [Fact]
        public void ValiderCollisions_Devrait_Retirer_ElementGraphique_Detruit_De_Liste_Quand_Heurter_Ennemi()
        {
            // Permet de tester toutes les combinaisons de collisions possibles non tester pr�c�demment pour un ennemi
            // Arrange
            List<int> lstNbElementsGraphiquesApresCollision = new List<int>();

            List<ElementGraphique> lstElementsGraphiquesAHeurter = new List<ElementGraphique>
            {
                new Asteroide(Jeu.LARGEUR_ECRAN / 2, 200),
                new CapsuleEnergie(Jeu.LARGEUR_ECRAN / 2, 200),
                new Ennemi(Jeu.LARGEUR_ECRAN / 2, 200)
            };

            for (int i = 0; i < lstElementsGraphiquesAHeurter.Count; i++)
            {
                Joueur joueur1 = new Joueur("Test");
                Jeu jeu1 = new Jeu(joueur1);

                jeu1.LstElementsGraphiques.Clear();
                jeu1.LstElementsGraphiques.Add(joueur1.Vaisseau);
                jeu1.LstElementsGraphiques.Add(lstElementsGraphiquesAHeurter[i]);
                jeu1.LstElementsGraphiques.Add(new Boss(Jeu.LARGEUR_ECRAN / 2 - 38, 0));
                jeu1.LstElementsGraphiques.Add(new Ennemi(Jeu.LARGEUR_ECRAN / 2, 200));

                // Act
                jeu1.ValiderCollisions();
                lstNbElementsGraphiquesApresCollision.Add(jeu1.LstElementsGraphiques.Count);
            }

            // Cas ennemi heurte le boss
            Joueur joueur2 = new Joueur("Test");
            Jeu jeu2 = new Jeu(joueur2);

            jeu2.LstElementsGraphiques.Clear();
            jeu2.LstElementsGraphiques.Add(joueur2.Vaisseau);
            jeu2.LstElementsGraphiques.Add(new Boss(Jeu.LARGEUR_ECRAN / 2, 0));
            jeu2.LstElementsGraphiques.Add(new Ennemi(Jeu.LARGEUR_ECRAN / 2, 0));

            // Act
            jeu2.ValiderCollisions();
            lstNbElementsGraphiquesApresCollision.Add(jeu2.LstElementsGraphiques.Count);

            // Assert
            Assert.Equal(new List<int> { 2, 2, 2, 2 }, lstNbElementsGraphiquesApresCollision);
        }

        [Fact]
        public void ValiderCollisions_Devrait_Retirer_ElementGraphique_Detruit_De_Liste_Quand_Heurter_Asteroide()
        {
            // Permet de tester toutes les combinaisons de collisions possibles non tester pr�c�demment pour un ast�ro�de
            // Arrange
            List<int> lstNbElementsGraphiquesApresCollision = new List<int>();

            List<ElementGraphique> lstElementsGraphiquesAHeurter = new List<ElementGraphique>
            {
                new Asteroide(Jeu.LARGEUR_ECRAN / 2, 200),
                new CapsuleEnergie(Jeu.LARGEUR_ECRAN / 2, 200),
            };

            for (int i = 0; i < lstElementsGraphiquesAHeurter.Count; i++)
            {
                Joueur joueur1 = new Joueur("Test");
                Jeu jeu1 = new Jeu(joueur1);

                jeu1.LstElementsGraphiques.Clear();
                jeu1.LstElementsGraphiques.Add(joueur1.Vaisseau);
                jeu1.LstElementsGraphiques.Add(lstElementsGraphiquesAHeurter[i]);
                jeu1.LstElementsGraphiques.Add(new Boss(Jeu.LARGEUR_ECRAN / 2 - 38, 0));
                jeu1.LstElementsGraphiques.Add(new Asteroide(Jeu.LARGEUR_ECRAN / 2, 200));

                // Act
                jeu1.ValiderCollisions();
                lstNbElementsGraphiquesApresCollision.Add(jeu1.LstElementsGraphiques.Count);
            }

            // Cas ast�ro�de heurte le boss
            Joueur joueur2 = new Joueur("Test");
            Jeu jeu2 = new Jeu(joueur2);

            jeu2.LstElementsGraphiques.Clear();
            jeu2.LstElementsGraphiques.Add(joueur2.Vaisseau);
            jeu2.LstElementsGraphiques.Add(new Boss(Jeu.LARGEUR_ECRAN / 2, 0));
            jeu2.LstElementsGraphiques.Add(new Asteroide(Jeu.LARGEUR_ECRAN / 2, 0));

            // Act
            jeu2.ValiderCollisions();
            lstNbElementsGraphiquesApresCollision.Add(jeu2.LstElementsGraphiques.Count);

            // Assert
            Assert.Equal(new List<int> { 2, 2, 1 }, lstNbElementsGraphiquesApresCollision);
        }

        [Fact]
        public void ValiderCollisions_Devrait_Retirer_ElementGraphique_Detruit_De_Liste_Quand_Heurter_CapsuleEnergie()
        {
            // Permet de tester toutes les combinaisons de collisions possibles non tester pr�c�demment pour un ast�ro�de
            // Arrange
            List<int> lstNbElementsGraphiquesApresCollision = new List<int>();

            // Cas capsule d'�nergie heurte une autre capsule d'�nergie
            Joueur joueur1 = new Joueur("Test");
            Jeu jeu1 = new Jeu(joueur1);
            jeu1.LstElementsGraphiques.Clear();
            jeu1.LstElementsGraphiques.Add(joueur1.Vaisseau);
            jeu1.LstElementsGraphiques.Add(new CapsuleEnergie(Jeu.LARGEUR_ECRAN / 2, 200));
            jeu1.LstElementsGraphiques.Add(new Boss(Jeu.LARGEUR_ECRAN / 2 - 38, 0));
            jeu1.LstElementsGraphiques.Add(new CapsuleEnergie(Jeu.LARGEUR_ECRAN / 2, 200));

            // Act
            jeu1.ValiderCollisions();
            lstNbElementsGraphiquesApresCollision.Add(jeu1.LstElementsGraphiques.Count);

            // Cas capsule d'�nergie heurte le boss
            Joueur joueur2 = new Joueur("Test");
            Jeu jeu2 = new Jeu(joueur2);

            jeu2.LstElementsGraphiques.Clear();
            jeu2.LstElementsGraphiques.Add(joueur2.Vaisseau);
            jeu2.LstElementsGraphiques.Add(new Boss(Jeu.LARGEUR_ECRAN / 2, 0));
            jeu2.LstElementsGraphiques.Add(new CapsuleEnergie(Jeu.LARGEUR_ECRAN / 2, 0));

            // Act
            jeu2.ValiderCollisions();
            lstNbElementsGraphiquesApresCollision.Add(jeu2.LstElementsGraphiques.Count);

            // Assert
            Assert.Equal(new List<int> { 2, 2 }, lstNbElementsGraphiquesApresCollision);
        }

        [Fact]
        public void
            ValiderCollisions_Devrait_Augmenter_Pointage_Du_Joueur_Quand_ElementGraphique_Detruit_Est_Implemente_Par_IPoint()
        {
            // Arrange
            Joueur joueur1 = new Joueur("Test");
            Jeu jeu1 = new Jeu(joueur1);
            jeu1.LstElementsGraphiques.Clear();
            jeu1.LstElementsGraphiques.Add(joueur1.Vaisseau);

            Joueur joueur2 = new Joueur("Test");
            Jeu jeu2 = new Jeu(joueur2);
            jeu2.LstElementsGraphiques.Clear();
            jeu2.LstElementsGraphiques.Add(joueur2.Vaisseau);

            List<uint> lstPointage = new List<uint>();

            // Cas missile du vaisseau du joueur d�truit un ennemi
            jeu1.LstElementsGraphiques.Add(new Missile(50, 50, Direction.Haut, 2));
            jeu1.LstElementsGraphiques.Add(new Ennemi(50, 50));
            // Act
            jeu1.ValiderCollisions();
            lstPointage.Add(jeu1.Joueur.Pointage);


            // Cas missile du vaisseau du joueur d�truit un boss
            jeu2.LstElementsGraphiques.Add(new Boss(50, 50));

            for (int i = 0; i < 20; i++)
            {
                jeu2.LstElementsGraphiques.Add(new Missile(50, 50, Direction.Haut, 2));
                // Act
                jeu2.ValiderCollisions();
            }

            lstPointage.Add(jeu2.Joueur.Pointage);

            Assert.Equal(new List<uint> { 5, 25 }, lstPointage);
        }

        [Fact]
        public void
            AnimerBots_Devrait_Deplacer_Tout_ElementGraphique_Dans_Liste_Quand_Est_Implemente_Par_IAutomatisable()
        {
            // Arrange
            Jeu jeu = new Jeu(new Joueur("Test"));
            jeu.LstElementsGraphiques.Clear();

            List<Point> lstPointsFinal = new List<Point>();

            jeu.LstElementsGraphiques.Add(new Ennemi(50, 50));
            jeu.LstElementsGraphiques.Add(new Boss(50, 0));
            jeu.LstElementsGraphiques.Add(new Asteroide(50, 50));
            jeu.LstElementsGraphiques.Add(new CapsuleEnergie(50, 50));

            jeu.AnimerBots();

            for (int i = 0; i < jeu.LstElementsGraphiques.Count; i++)
                lstPointsFinal.Add(jeu.LstElementsGraphiques[i].Position);

            Assert.NotEqual(new List<Point>
            {
                new Point(50, 50),
                new Point(50, 50),
                new Point(50, 50),
                new Point(50, 50),
            }, lstPointsFinal);
        }
    }
}