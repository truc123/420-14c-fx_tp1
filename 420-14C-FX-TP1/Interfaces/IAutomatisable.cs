﻿#region MÉTADONNÉES

// Nom du fichier : IAutomatisable.cs
// Auteur : (1933760)
// Date de création : 2021-02-17
// Date de modification : 2021-02-20

#endregion

#region USING

using _420_14C_FX_TP1.Classes;

#endregion

namespace _420_14C_FX_TP1.Interfaces
{
    /// <summary>
    /// Ce comportement permet à l'objet d'être animé de manière automatique
    /// </summary>
    public interface IAutomatisable
    {
        #region MÉTHODE

        /// <summary>
        /// Permet d'animer l'objet
        /// </summary>
        /// <returns>Retourne un élément graphique pouvant contenir un projetile ou être nul</returns>
        public ElementGraphique Animer();

        #endregion
    }
}