﻿#region MÉTADONNÉES

// Nom du fichier : IEnergie.cs
// Auteur : Mélina Hotte (1933760)
// Date de création : 2021-02-17
// Date de modification : 2021-02-20

#endregion

namespace _420_14C_FX_TP1.Interfaces
{
    /// <summary>
    /// Inque qu'un objet possède de l'énergie
    /// </summary>
    public interface IEnergie
    {
        #region PROPRIÉTÉ ET INDEXEUR

        /// <summary>
        /// Points d'énergie d'un objet
        /// </summary>
        public int Energie { get; set; }

        #endregion
    }
}