﻿#region MÉTADONNÉES

// Nom du fichier : IDommageable.cs
// Auteur : Mélina Hotte (1933760)
// Date de création : 2021-02-17
// Date de modification : 2021-02-20

#endregion

#region USING

using _420_14C_FX_TP1.Classes;

#endregion

namespace _420_14C_FX_TP1.Interfaces
{
    /// <summary>
    /// Ce comportement permet de diminuer les points de vie d'un élément entré en collision avec l'objet dommageable.
    /// </summary>
    public interface IDommageable
    {
        #region PROPRIÉTÉ ET INDEXEUR

        /// <summary>
        /// Nombre de points de dommage à retirer à un objet.
        /// </summary>
        public uint Dommage { get; set; }

        #endregion

        #region MÉTHODE

        /// <summary>
        /// Permet d'endommager l'élément graphique reçu en paramètre.
        /// </summary>
        /// <param name="pElementGraphique">Élément graphique à endommager</param>
        public void Endommager(ElementGraphique pElementGraphique);

        #endregion
    }
}