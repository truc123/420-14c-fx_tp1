﻿#region MÉTADONNÉES

// Nom du fichier : IPoint.cs
// Auteur : Mélina Hotte (1933760)
// Date de création : 2021-02-20
// Date de modification : 2021-02-21

#endregion

namespace _420_14C_FX_TP1.Interfaces
{
    /// <summary>
    /// Indique qu'un objet possède des points à ajouter au pointage du joueur lorsqu'il est détruit
    /// </summary>
    public interface IPoint
    {
        #region PROPRIÉTÉ ET INDEXEUR

        /// <summary>
        /// Valeur en points d'un objet
        /// </summary>
        public uint Points { get; set; }

        #endregion
    }
}