﻿#region MÉTADONNÉES

// Nom du fichier : IEnergiser.cs
// Auteur : Mélina Hotte (1933760)
// Date de création : 2021-02-17
// Date de modification : 2021-02-20

#endregion

#region USING

using _420_14C_FX_TP1.Classes;

#endregion

namespace _420_14C_FX_TP1.Interfaces
{
    /// <summary>
    /// Ce comportement permet d'énergiser un élément graphique avec lequqel il entre en collision
    /// </summary>
    public interface IEnergiser
    {
        #region PROPRIÉTÉ ET INDEXEUR

        /// <summary>
        /// Nombre de points d'énergie à donner à un élément graphique
        /// </summary>
        public int PointsEnergie { get; set; }

        #endregion

        #region MÉTHODE

        /// <summary>
        /// Permet d'énergiser l'élément graphique reçu en paramètre
        /// </summary>
        /// <param name="pElementGraphique">Élément graphique à énergiser</param>
        public void Energiser(ElementGraphique pElementGraphique);

        #endregion
    }
}