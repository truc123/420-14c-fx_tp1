﻿#region MÉTADONNÉES

// Nom du fichier : frmJoueur.cs
// Auteur : Mélina Hotte (1933760)
// Date de création : 2021-02-05
// Date de modification : 2021-02-20

#endregion

#region USING

using _420_14C_FX_TP1.Classes;
using System;
using System.Diagnostics;
using System.Windows.Forms;

#endregion

namespace _420_14C_FX_TP1
{
    /// <summary>
    /// Classe permettant d'afficher le formulaire de connexion du joueur
    /// </summary>
    public partial class FrmJoueur : Form
    {
        #region ATTRIBUT

        /// <summary>
        /// Joueur
        /// </summary>
        private Joueur _joueur;

        #endregion

        #region PROPRIÉTÉ ET INDEXEUR

        /// <summary>
        /// Obtient ou définit le joueur
        /// </summary>
        /// <exception cref="ArgumentNullException">Lancée lorsque le joueur est nul</exception>
        public Joueur Joueur
        {
            get { return _joueur; }
            set
            {
                if (value is null)
                    throw new ArgumentNullException("Joueur", "Le joueur ne peut pas être nul.");
                _joueur = value;
            }
        }

        #endregion

        #region CONSTRUCTEUR

        /// <summary>
        /// Constructeur du formulaire Joueur
        /// </summary>
        public FrmJoueur()
        {
            InitializeComponent();
        }

        #endregion

        #region MÉTHODES

        /// <summary>
        /// Permet d'effectuer le chargement du formulaire.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmJoueur_Load(object sender, EventArgs e)
        {
            AcceptButton = btnJouer;
        }

        /// <summary>
        /// Permet la validation du formulaire
        /// </summary>
        /// <remarks>Les erreurs doivent être ajoutées au contrôle ErrorProvider du formulaire et faire afficher un
        /// message indiquant les erreurs à corriger.</remarks>
        /// <returns>Vrai ou Faux selon la validité du formulaire</returns>
        private bool ValiderFormulaire()
        {
            errorProvider.Clear();

            txtNom.Text = txtNom.Text.Trim();

            if (string.IsNullOrEmpty(txtNom.Text))
                errorProvider.SetError(txtNom, "Veuillez saisir un nom.");
            else
            {
                int nbCharPourChampsNom = txtNom.Text.Length;

                if (nbCharPourChampsNom < 5 || nbCharPourChampsNom > 20)
                    errorProvider.SetError(txtNom, "Le nom doit contenir entre 5 et 20 caractères.");
            }

            string erreur = errorProvider.GetError(txtNom);
            if (!string.IsNullOrWhiteSpace(erreur))
            {
                MessageBox.Show($"Veuillez corriger l'erreur suivante : {erreur}", "Nom du joueur invalide");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Permet de quitter le formulaire en indiquant que le résultat du dialogue est 'Annuler'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAnnuler_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Permet de créer un joueur si le formulaire est valide
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnJouer_Click(object sender, EventArgs e)
        {
            try
            {
                if (ValiderFormulaire())
                {
                    Joueur = new Joueur(txtNom.Text);
                    DialogResult = DialogResult.OK;
                }
                else
                    DialogResult = DialogResult.None;
            }
            catch (Exception msgErreur)
            {
                MessageBox.Show(
                    $"Une erreur s'est produite : {msgErreur}\n\nVeuillez communiquer avec l'administrateur du système.",
                    "FrmJoueur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ErreurLog.Journaliser($"BtnJouer_Click : {msgErreur}", TraceEventType.Critical, 2);
            }
        }

        #endregion
    }
}