﻿#region MÉTADONNÉES

// Nom du fichier : frmJeu.cs
// Auteur : Mélina Hotte (1933760)
// Date de création : 2021-02-05
// Date de modification : 2021-02-26

#endregion

#region USING

using _420_14C_FX_TP1.Classes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;

#endregion

namespace _420_14C_FX_TP1
{
    public partial class FrmJeu : Form
    {
        #region ATTRIBUTS

        /// <summary>
        /// Permet la gestion du jeu
        /// </summary>
        private Jeu _jeu;

        /// <summary>
        /// Direction du déplacement du joueur
        /// </summary>
        private Direction _directionCourante = Direction.Aucune;

        /// <summary>
        /// Accessibilité de la base de donnée
        /// </summary>
        private bool _baseDonneesEstAccessible;

        #endregion

        #region CONSTRUCTEURS

        /// <summary>
        /// Constructeur du formulaire Jeu
        /// </summary>
        public FrmJeu()
        {
            InitializeComponent();
        }

        #endregion

        #region MÉTHODES

        /// <summary>
        /// Permet d'effectuer le chargement du formulaire.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmJeu_Load(object sender, EventArgs e)
        {
            //Permet de capturer les touches clavier par le formulaire.
            KeyPreview = true;

            //On désactive la navigation de tous les contrôles par les flèches et la barre d'espacement.
            DesactiverNavigation(Controls);

            //Ajustement de la taille de l'écran du jeu
            pnlEcran.Width = Jeu.LARGEUR_ECRAN;
            pnlEcran.Height = Jeu.HAUTEUR_ECRAN;

            //Initialisation du formulaire
            InitialiserFormulaire();

            //Affichage du classement des joueurs
            InitialiserClassement();
        }

        /// <summary>
        /// Permet la désactivation de la navigation par les fleches et la bar d'espacement
        /// </summary>
        /// <param name="controls">Contrôle à désactiver</param>
        private void DesactiverNavigation(Control.ControlCollection controls)
        {
            foreach (Control ctrl in controls)
            {
                ctrl.PreviewKeyDown += new PreviewKeyDownEventHandler(Controls_PreviewKeyDown);
                DesactiverNavigation(ctrl.Controls);
            }
        }

        /// <summary>
        /// Permet d'ignorer l'activation d'un contrôle du formulaire lorsque l'utilisateur appuie sur les fèches ou
        /// la barre d'espacement
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Controls_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                case Keys.Down:
                case Keys.Left:
                case Keys.Right:
                    e.IsInputKey = true;
                    break;
            }
        }

        /// <summary>
        /// Permet l'initialisation du fomulaire
        /// </summary>
        private void InitialiserFormulaire()
        {
            lblPointage.Text = "0";
            pgbEnergie.Value = 0;
        }

        /// <summary>
        /// Permet d'obtenir le classement des joueurs
        /// </summary>
        /// <remarks>
        /// Si une exception de type DatabaseException alors on indique à l'utilisateur que la base de donnnées
        /// est inaccessible et on journalise l'erreur en tant que type "Warning. Les autres sont journalisés dans le fichier de journalisation."
        /// </remarks>
        private void InitialiserClassement()
        {
            lstClassement.Items.Clear();

            try
            {
                List<Joueur> lstJoueurs = Classement.ObtenirClassement();

                foreach (Joueur joueur in lstJoueurs)
                    lstClassement.Items.Add(joueur);
            }
            catch (DatabaseException e)
            {
                MessageBox.Show(e.Message, "Initialisation du classement", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                _baseDonneesEstAccessible = false;
            }
            catch (Exception e)
            {
                FrmJeu.JournaliserErreur("InitialiserClassement", e.Message, TraceEventType.Critical, 2);
            }
        }

        /// <summary>
        /// Permet de changer la direction courante du vaisseau du joueur lorsque qu'une touche est pressée
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmJeu_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    //_gauche = true;
                    _directionCourante = Direction.Gauche;
                    break;
                case Keys.Right:
                    //_droite = true;
                    _directionCourante = Direction.Droite;
                    break;
            }
        }

        /// <summary>
        /// Permet de mettre la direction courante du vaisseau du joueur à 'Aucune' lorsque la touche est relâchée
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmJeu_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                case Keys.Right:
                    _directionCourante = Direction.Aucune;
                    break;
            }
        }

        /// <summary>
        /// Permet au vaisseau du joueur de faire feu lorsque la barre d'espace est pressée
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmJeu_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Space && _jeu != null)
                _jeu.LstElementsGraphiques.Add(_jeu.Joueur.Vaisseau.FaireFeu());
        }

        /// <summary>
        /// Permet de dessiner les éléments graphiques n'étant pas détruit alors que l'écran est rafraîchit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PnlEcran_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                if (!(_jeu is null))
                {
                    if (_jeu.Joueur.Vaisseau.Detruit)
                        _jeu.Joueur.Vaisseau.Dessiner(e.Graphics);
                    else
                        for (int i = 0; i < _jeu.LstElementsGraphiques.Count; i++)
                            _jeu.LstElementsGraphiques[i].Dessiner(e.Graphics);
                }
            }
            catch (Exception msg)
            {
                FrmJeu.JournaliserErreur("PnlEcran_Paint", msg.Message, TraceEventType.Critical, 1);
            }
        }

        /// <summary>
        /// Permet de commencer une nouvelle partie lorsque le bouton est pressé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnNouvellePartie_Click(object sender, EventArgs e)
        {
            tmrJeu.Stop();

            InitialiserFormulaire();

            FrmJoueur frmJoueur = new FrmJoueur();

            if (frmJoueur.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    _jeu = new Jeu(frmJoueur.Joueur);
                    _jeu.DemarrerPartie();

                    pgbEnergie.Value = _jeu.Joueur.Vaisseau.Energie;

                    //Démarrage du timer du jeu
                    tmrJeu.Interval = 1;
                    tmrJeu.Start();

                    //On déplace le focus du bouton pour ne plus l'activer avec la barre d'espacement
                    lblPoints.Focus();
                }
                catch (Exception exception)
                {
                    FrmJeu.JournaliserErreur("BtnNouvellePartie_Click", exception.Message, TraceEventType.Critical, 1);
                }
            }
            else
                MessageBox.Show("Il vous inscrire un nom de joueur pour pouvoir jouer.",
                    "Impossible de commencer la partie",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            frmJoueur.Dispose();
        }

        /// <summary>
        /// Permet de quitter le jeu en fermant l'application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnQuitter_Click(object sender, EventArgs e)
        {
            Close();
            Application.Exit();
        }

        /// <summary>
        /// Timer du jeu. Permet de déplacer le vaisseau du joueur et d'animer les objets dans la liste des éléments graphiques
        /// </summary>
        /// <remarks>
        /// Si une exception de type DatabaseException est levée, alors on indique à l'utilisateur que la base de données
        /// est inaccessible et l'on journalise l'erreur en tant que type "Warning" dans le fichier de journalisation.
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TrmJeu_Tick(object sender, EventArgs e)
        {
            //Déplacement du vaisseau
            _jeu.Joueur.Vaisseau.Deplacer(_directionCourante);

            //Animation des bots
            _jeu.AnimerBots();

            //Validation des collisions
            _jeu.ValiderCollisions();
            pgbEnergie.Value = _jeu.Joueur.Vaisseau.Energie;

            //Mise à jour des points du joueur
            lblPointage.Text = _jeu.Joueur.Pointage.ToString();

            //Ragraîchir l'écran
            pnlEcran.Refresh();

            // Affiche le message de fin de partie
            if (_jeu.LstElementsGraphiques.Count <= 1)
            {
                tmrJeu.Stop();

                if (_jeu.Joueur.Vaisseau.Detruit)
                    MessageBox.Show($"Vous avez perdu !", "Partie terminée", MessageBoxButtons.OK);
                else
                {
                    if (_baseDonneesEstAccessible)
                    {
                        if (MessageBox.Show($"Vous avez gagné !", "Partie terminée", MessageBoxButtons.YesNo) ==
                            DialogResult.Yes)
                            EnregistrerResultat();
                    }
                    else
                        MessageBox.Show($"Vous avez gagné !", "Partie terminée", MessageBoxButtons.OK);
                }
            }
        }

        /// <summary>
        /// Permet l'enregistrement du résultat du joueur dans la base de données
        /// <remarks>On demande à l'utilisateur s'il désire enregistrer son résultat</remarks>
        /// </summary>
        private void EnregistrerResultat()
        {
            try
            {
                Classement.EnregistrerResultat(_jeu.Joueur);
                InitialiserClassement();
            }
            catch (DatabaseException e)
            {
                MessageBox.Show(e.Message, "EnregistrerResultat", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
            catch (Exception e)
            {
                FrmJeu.JournaliserErreur("EnregistrerResultat", e.Message, TraceEventType.Critical, 2);
            }
        }

        /// <summary>
        /// Permet de journaliser une erreur
        /// </summary>
        /// <param name="pMethodeEnErreur">Nom de la méthode ayant provoquée l'erreur</param>
        /// <param name="msgErreur">Message de l'erreur</param>
        /// <param name="pType">Type de l'erreur</param>
        /// <param name="pId">Identifiant de l'erreur</param>
        private static void JournaliserErreur(string pMethodeEnErreur, string msgErreur, TraceEventType pType, int pId)
        {
            MessageBox.Show(
                $"Une erreur s'est produite : {msgErreur}\n\nVeuillez communiquer avec l'administrateur du système.",
                "FrmJeu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            ErreurLog.Journaliser($"{pMethodeEnErreur} : {msgErreur}", pType, pId);
        }

        #endregion
    }
}