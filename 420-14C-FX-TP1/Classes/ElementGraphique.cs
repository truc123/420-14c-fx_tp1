﻿#region MÉTADONNÉES

// Nom du fichier : ElementGraphique.cs
// Auteur : Mélina Hotte (1933760)
// Date de création : 2021-02-15
// Date de modification : 2021-02-26

#endregion

#region USING

using _420_14C_FX_TP1.Interfaces;
using System.Drawing;

#endregion

namespace _420_14C_FX_TP1.Classes
{
    /// <summary>
    /// Représente un élément graphique
    /// </summary>
    public abstract class ElementGraphique
    {
        #region ATTRIBUTS

        /// <summary>
        /// Position de l'élément graphique
        /// </summary>
        private Point _position;

        /// <summary>
        /// Rectangle de l'élément graphique
        /// </summary>
        private Rectangle _rectangle;

        /// <summary>
        /// Vitesse de l'élément graphique
        /// </summary>
        private int _vitesse;

        /// <summary>
        /// Direction courante de l'élément graphique
        /// </summary>
        private Direction _directionCourante;

        /// <summary>
        /// État détruit de l'élément graphique
        /// </summary>
        private bool _detruit;

        #endregion

        #region PROPRIÉTÉS ET INDEXEURS

        /// <summary>
        /// Obtient ou défini la position de l'élément graphique
        /// </summary>
        public Point Position
        {
            get { return _position; }
            protected set { _position = value; }
        }

        /// <summary>
        /// Obtient ou défini le rectangle de l'élément graphique
        /// </summary>
        public Rectangle Rectangle
        {
            get { return _rectangle; }
            protected set { _rectangle = value; }
        }

        /// <summary>
        /// Obtient ou défini la vitesse de déplacement de l'élément graphique
        /// </summary>
        public int Vitesse
        {
            get { return _vitesse; }
            protected set { _vitesse = value; }
        }

        /// <summary>
        /// Obtient ou défini la direction courante de l'élément graphique
        /// </summary>
        public Direction DirectionCourante
        {
            get { return _directionCourante; }
            protected set { _directionCourante = value; }
        }

        /// <summary>
        /// Obtient ou défini l'état détruit de l'élément graphique
        /// </summary>
        public virtual bool Detruit
        {
            get { return _detruit; }
            protected set { _detruit = value; }
        }

        #endregion

        #region CONSTRUCTEUR

        /// <summary>
        /// Permet de créer un élément graphique
        /// </summary>
        /// <param name="pX">Position dans l'axe X</param>
        /// <param name="pY">Position dans l'axe Y</param>
        protected ElementGraphique(int pX, int pY)
        {
            Position = new Point(pX, pY);
        }

        #endregion

        #region MÉTHODES

        /// <summary>
        /// Permet de dessiner graphiquement l'élément graphique
        /// </summary>
        /// <param name="pGraphics"></param>
        public abstract void Dessiner(Graphics pGraphics);

        /// <summary>
        /// Permet de déplacement l'élément graphique selon sa direction et sa vitesse
        /// </summary>
        /// <param name="pDirection">Direction courante du déplacement</param>
        protected void Deplacer(Direction pDirection)
        {
            switch (pDirection)
            {
                case Direction.Gauche:
                    Position = new Point(Position.X - Vitesse, Position.Y);
                    break;

                case Direction.Droite:
                    Position = new Point(Position.X + Vitesse, Position.Y);
                    break;

                case Direction.Haut:
                    Position = new Point(Position.X, Position.Y - Vitesse);
                    break;

                case Direction.Bas:
                    Position = new Point(Position.X, Position.Y + Vitesse);
                    break;
            }
        }

        /// <summary>
        /// Détermine l'action à effectuer à la suite d'une collision avec l'élément graphique reçu en paramètre
        /// </summary>
        /// <param name="pElementGraphique">Élément graphique qui a heurté l'objet</param>
        public virtual void HeurterPar(ElementGraphique pElementGraphique)
        {
            if (!(this is IEnergie))
            {
                if (pElementGraphique is IEnergie objPourIEnergie)
                {
                    if (this is IDommageable objThisIDommageable)
                        objThisIDommageable.Endommager((ElementGraphique)objPourIEnergie);
                    else if (this is IEnergiser objThisIEnergiser)
                        objThisIEnergiser.Energiser((ElementGraphique)objPourIEnergie);
                }
                else
                    pElementGraphique.Detruit = true;

                Detruit = true;
            }
            else
            {
                if (pElementGraphique is IDommageable objPourIDommageable)
                    objPourIDommageable.Endommager(this);
                else if (pElementGraphique is IEnergiser objPourIEnergiser)
                    objPourIEnergiser.Energiser(this);

                pElementGraphique.Detruit = true;
            }
        }

        /// <summary>
        /// Vérifie si l'élément graphique est visible dans l'écran du jeu
        /// </summary>
        /// <returns>Vrai ou faux selon la visibilité de l'élément graphique</returns>
        protected bool VerifierVisibilite()
        {
            bool estVisible;
            bool estVisiblePourAxeX = Position.X >= 0 && Position.X <= Jeu.LARGEUR_ECRAN - Rectangle.Width;

            if (DirectionCourante == Direction.Haut)
                estVisible = Position.Y + Rectangle.Height >= 0 && estVisiblePourAxeX;
            else if (DirectionCourante == Direction.Bas)
                estVisible = Position.Y <= Jeu.HAUTEUR_ECRAN && estVisiblePourAxeX;
            else
                estVisible = estVisiblePourAxeX;

            return estVisible;
        }

        #endregion
    }
}