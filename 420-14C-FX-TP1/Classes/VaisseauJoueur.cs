﻿#region MÉTADONNÉES

// Nom du fichier : VaisseauJoueur.cs
// Auteur : Mélina Hotte (1933760)
// Date de création : 2021-02-15
// Date de modification : 2021-02-24

#endregion

#region USING

using _420_14C_FX_TP1.Interfaces;
using _420_14C_FX_TP1.Properties;
using System.Drawing;

#endregion

namespace _420_14C_FX_TP1.Classes
{
    /// <summary>
    /// Représente un vaisseau d'un joueur
    /// </summary>
    public class VaisseauJoueur : ElementGraphique, IEnergie
    {
        #region CONSTANTES ET ATTRIBUTS STATIQUES

        /// <summary>
        /// Hauteur du vaisseau
        /// </summary>
        private const int HAUTEUR = 50;

        /// <summary>
        /// Largeur du vaisseau
        /// </summary>
        private const int LARGEUR = 50;

        /// <summary>
        /// Énergie maximale du vaisseau
        /// </summary>
        public const int ENERGIE_MAX = 10;

        /// <summary>
        /// Vitesse de déplacement du vaisseau
        /// </summary>
        private const int VITESSE = 5;

        /// <summary>
        /// Hauteur de l'explosion du vaisseau
        /// </summary>
        private const int EXPLOSION_HAUTEUR = 100;

        /// <summary>
        /// Largeur de l'exposion du vaisseau
        /// </summary>
        private const int EXPLOSION_LARGEUR = 100;

        /// <summary>
        /// Direction du déplacement par défaut du vaisseau
        /// </summary>
        private const Direction DIRECTION_DEFAUT = Direction.Aucune;

        #endregion

        #region ATTRIBUTS

        /// <summary>
        /// Énergie du vaisseau du joueur
        /// </summary>
        private int _energie;

        #endregion

        #region PROPRIÉTÉS ET INDEXEURS

        /// <summary>
        /// Obtient ou défini la quantité d'énergie du vaisseau
        /// </summary>
        public int Energie
        {
            get { return _energie; }
            set
            {
                if (value <= 0)
                {
                    value = 0;
                    Detruit = true;
                }
                else if (value > VaisseauJoueur.ENERGIE_MAX)
                    value = VaisseauJoueur.ENERGIE_MAX;

                _energie = value;
            }
        }

        #endregion

        #region CONSTRUCTEURS

        /// <summary>
        /// Permet de créé un vaisseau
        /// </summary>
        /// <param name="pX">Position dans l'axe X</param>
        /// <param name="pY">Position dans l'axe Y</param>
        public VaisseauJoueur(int pX, int pY) : base(pX, pY)
        {
            DirectionCourante = VaisseauJoueur.DIRECTION_DEFAUT;
            Vitesse = VaisseauJoueur.VITESSE;
            Energie = VaisseauJoueur.ENERGIE_MAX;

            Rectangle = new Rectangle(Position.X, Position.Y, VaisseauJoueur.LARGEUR, VaisseauJoueur.HAUTEUR);
        }

        #endregion

        #region MÉTHODES

        /// <summary>
        /// Permet de dessiner graphiquement le vaisseau
        /// </summary>
        /// <param name="pGraphics"></param>
        public override void Dessiner(Graphics pGraphics)
        {
            // L'on vérifie si le vaisseau n'est pas dans l'écran pour pouvoir modifier sa position pour que ce soit le cas afin que celui-ci n'ait pas à l'extérieur 
            if (!VerifierVisibilite())
                Position = new Point(Position.X <= 0
                        ? 0
                        : Position.X >= Jeu.LARGEUR_ECRAN - VaisseauJoueur.LARGEUR
                            ? Jeu.LARGEUR_ECRAN - Rectangle.Width
                            : Position.X
                    , Position.Y);

            pGraphics.DrawImage(new Bitmap(Resources.vaisseau, Rectangle.Width, Rectangle.Height), Position);
            Rectangle = new Rectangle(Position.X, Position.Y, Rectangle.Width, Rectangle.Height);

            if (Detruit)
                pGraphics.DrawImage(
                    new Bitmap(Resources.explosion, VaisseauJoueur.EXPLOSION_LARGEUR, VaisseauJoueur.EXPLOSION_HAUTEUR),
                    Position.X - 25, Position.Y - 50);
        }

        /// <summary>
        /// Permet de déplacer le vaisseau selon la direction reçue en paramètre
        /// </summary>
        /// <param name="pDirection">Direction du déplacement</param>
        /// <remarks>
        /// Pour faire bouger le vaisseau, l'on crée une nouvelle fonction à partir de celle héritée,
        /// car celle-ci est protégée et ne peut pas être utilisée en dehors des classes qui hérite de la classe mère.
        /// </remarks>
        public new void Deplacer(Direction pDirection)
        {
            base.Deplacer(pDirection);
        }

        /// <summary>
        /// Détermine l'action à effectuer suite à la collision avec l'élément graphique reçu en paramètre pour le vaisseau du joueur
        /// </summary>
        /// <param name="pElementGraphique"></param>
        public override void HeurterPar(ElementGraphique pElementGraphique)
        {
            if (pElementGraphique is IPoint objPourIPoint && !(pElementGraphique is IEnergie))
                objPourIPoint.Points += 1;

            base.HeurterPar(pElementGraphique);
        }

        /// <summary>
        /// Permet au vaisseau de faire feu et ainsi créé un missile
        /// </summary>
        /// <returns>Retourne un missile à la position actuelle du vaisseau</returns>
        public Missile FaireFeu()
        {
            return new Missile(Position.X + 20, Position.Y - 10, Direction.Haut, 2);
        }

        #endregion
    }
}