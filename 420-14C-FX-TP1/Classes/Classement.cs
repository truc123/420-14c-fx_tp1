﻿#region MÉTADONNÉES

// Nom du fichier : Classement.cs
// Auteur : Mélina Hotte (1933760)
// Date de création : 2021-02-15
// Date de modification : 2021-02-26

#endregion

#region USING

using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

#endregion

namespace _420_14C_FX_TP1.Classes
{
    /// <summary>
    /// Classe permettant la gestion du classement dans la base de données
    /// </summary>
    public static class Classement
    {
        #region CONSTANTES ET ATTRIBUTS STATIQUES

        /// <summary>
        /// Chaîne de connexion à la base de donnnées
        /// </summary>
        private const string CONNECTION_STRING_NAME = "MySql";

        #endregion

        #region MÉTHODES

        /// <summary>
        /// Permet de créer une connexion à partir de la chaine de connexion contenue dans le fichier app.config
        /// </summary>
        /// <returns>Une connexion à MySql</returns>
        /// <exception cref="DatabaseException">Lancé lorsqu'une erreur en lien avec la base de données se produit</exception>
        private static MySqlConnection CreerConnection()
        {
            try
            {
                //Lecture de la chaîne de connexion dans le app.config
                string connectionString = ConfigurationManager.ConnectionStrings[Classement.CONNECTION_STRING_NAME]
                    .ConnectionString;

                //Instanciation de la connexion
                return new MySqlConnection(connectionString);
            }
            catch (Exception e)
            {
                throw new DatabaseException($"Méthode : CreerConnection - Exception : {e.Message}");
            }
        }

        /// <summary>
        /// Permet d'obtenir le classement des joueurs
        /// </summary>
        /// <returns>Liste de joueurs</returns>
        public static List<Joueur> ObtenirClassement()
        {
            //Connexion
            MySqlConnection cn = Classement.CreerConnection();

            //Création et la de la liste des joueurs
            List<Joueur> joueurs = new List<Joueur>();

            try
            {
                //Ouverture de la connexion
                cn.Open();

                //Requête pour obtenir la liste des joueurs triée en ordre de pointage et de nom
                //Préparation de la commande
                MySqlCommand cmd = new MySqlCommand("SELECT * FROM `classement` ORDER BY Pointage, Nom DESC", cn);

                //Exécution de la commande
                MySqlDataReader dr = cmd.ExecuteReader();

                //Ajout des joueurs à la liste
                while (dr.Read())
                    joueurs.Add(new Joueur(dr.GetString(1), dr.GetUInt32(2), dr.GetDateTime(3)));
            }
            catch (Exception e)
            {
                throw new DatabaseException($"Méthode : ObtenirClassement - Exception : {e.Message}");
            }
            finally
            {
                //Fermeture de la connexion
                if (cn != null && cn.State == ConnectionState.Open)
                {
                    cn.Close();
                    cn.Dispose();
                }
            }

            return joueurs;
        }

        /// <summary>
        /// Permet l'enregistrement du résultat d'un joueur dans la base de données
        /// </summary>
        /// <param name="pJoueur">Joueur du jeu en cours</param>
        /// <exception cref="DatabaseException">Lancé lorsqu'une erreur en lien avec la base de données se produit</exception>
        public static void EnregistrerResultat(Joueur pJoueur)
        {
            if (pJoueur == null)
                throw new ArgumentNullException("pJoueur", "Le joueur ne doit pas être nul.");

            //Connexion
            MySqlConnection cn = Classement.CreerConnection();

            try
            {
                //Ouverture de la connexion
                cn.Open();

                //Requête pour obtenir l'ajout d'un résultat au classement
                string requete = "SELECT Nom FROM classement WHERE Nom = @nom";

                //Préparation de la commande avec paramètres
                MySqlCommand cmd = new MySqlCommand(requete, cn);

                cmd.Parameters.AddWithValue("@nom", pJoueur.Nom);

                MySqlDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                    requete = "UPDATE classement SET Pointage = @pointage WHERE nom = @nom;";
                else
                    requete =
                        "INSERT INTO classement (Nom, Pointage, DateResultat) VALUES (@nom, @pointage, @dateResultat);";

                dr.Close();
                dr.Dispose();

                cmd.CommandText = requete;
                cmd.Parameters.AddWithValue("@pointage", pJoueur.Pointage);
                cmd.Parameters.AddWithValue("@dateResultat", pJoueur.DateResultat);
                //Exécution de la commande
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new DatabaseException($"Méthode : EnregistrerResultat - Exception : {e.Message}");
            }
            finally
            {
                //Fermeture de la connexion
                if (cn != null && cn.State == ConnectionState.Open)
                {
                    cn.Close();
                    cn.Dispose();
                }
            }
        }

        #endregion
    }
}