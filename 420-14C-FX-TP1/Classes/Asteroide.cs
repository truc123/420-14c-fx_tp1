﻿#region MÉTADONNÉES

// Nom du fichier : Asteroide.cs
// Auteur : Mélina Hotte (1933760)
// Date de création : 2021-02-15
// Date de modification : 2021-02-21

#endregion

#region USING

using _420_14C_FX_TP1.Interfaces;
using _420_14C_FX_TP1.Properties;
using System.Drawing;

#endregion

namespace _420_14C_FX_TP1.Classes
{
    /// <summary>
    /// Représente un astéroïde
    /// </summary>
    public class Asteroide : ElementGraphique, IAutomatisable, IDommageable
    {
        #region CONSTANTES ET ATTRIBUTS STATIQUES

        /// <summary>
        /// Hauteur de l'astéroïde
        /// </summary>
        private const int HAUTEUR = 25;

        /// <summary>
        /// Largeur de l'astéroïde
        /// </summary>
        private const int LARGEUR = 25;

        /// <summary>
        /// Vitesse de déplacement de l'astéroïde
        /// </summary>
        private const int VITESSE = 2;

        /// <summary>
        /// Points de dommage de l'astéroïde
        /// </summary>
        public const int DOMMAGE = 20;

        #endregion

        #region ATTRIBUT
        /// <summary>
        /// Nombre de points de dommage à retirer à un élément graphique possédant de l'énergie
        /// </summary>
        private uint _dommage;

        #endregion

        #region PROPRIÉTÉ ET INDEXEUR

        /// <summary>
        /// Obtient ou défini le nombre de points d'énergie à diminuer
        /// </summary>
        public uint Dommage
        {
            get { return _dommage; }
            set { _dommage = value; }
        }

        #endregion

        #region CONSTRUCTEUR

        /// <summary>
        /// Permet de créer un astéroïde
        /// </summary>
        /// <param name="pX">Position dans l'axe X</param>
        /// <param name="pY">Position dans l'axe Y</param>
        public Asteroide(int pX, int pY) : base(pX, pY)
        {
            Vitesse = Asteroide.VITESSE;
            Dommage = Asteroide.DOMMAGE;
            DirectionCourante = Direction.Bas;

            Rectangle = new Rectangle(Position.X, Position.Y, Asteroide.LARGEUR, Asteroide.HAUTEUR);
        }

        #endregion

        #region MÉTHODES

        /// <summary>
        /// Permet de dessiner graphiquement l'astéroïde
        /// </summary>
        /// <param name="pGraphics"></param>
        public override void Dessiner(Graphics pGraphics)
        {
            if (DirectionCourante == Direction.Bas && Position.Y + Asteroide.HAUTEUR >= 0)
                if (VerifierVisibilite())
                {
                    pGraphics.DrawImage(new Bitmap(Resources.asteroide, Rectangle.Width, Rectangle.Height), Position);
                    Rectangle = new Rectangle(Position.X, Position.Y, Rectangle.Width, Rectangle.Height);
                }
                else
                    Detruit = true;

        }

        /// <summary>
        /// Permet d'endommager l'élément graphique reçu en paramètre pour l'astéroïde
        /// </summary>
        /// <param name="pElementGraphique"></param>
        public void Endommager(ElementGraphique pElementGraphique)
        {
            ((IEnergie)pElementGraphique).Energie -= (int)Dommage;
        }

        /// <summary>
        /// Permet d'animer l'astéroïde en le déplaçant
        /// </summary>
        /// <returns>Retourne toujours nul</returns>
        public ElementGraphique Animer()
        {
            Deplacer(DirectionCourante);
            return null;
        }

        #endregion
    }
}