﻿#region MÉTADONNÉES

// Nom du fichier : Boss.cs
// Auteur : Mélina Hotte (1933760)
// Date de création : 2021-02-15
// Date de modification : 2021-02-26

#endregion

#region USING

using _420_14C_FX_TP1.Interfaces;
using _420_14C_FX_TP1.Properties;
using System;
using System.Drawing;
using System.Windows.Forms;

#endregion

namespace _420_14C_FX_TP1.Classes
{
    /// <summary>
    /// Représente un ennemi final
    /// </summary>
    public class Boss : ElementGraphique, IPoint, IEnergie, IAutomatisable
    {
        #region CONSTANTES ET ATTRIBUTS STATIQUES

        /// <summary>
        /// Hauteur du boss
        /// </summary>
        private const int HAUTEUR = 76;

        /// <summary>
        /// Largeur du boss
        /// </summary>
        private const int LARGEUR = 76;

        /// <summary>
        /// Énergie maximale du boss
        /// </summary>
        public const int ENERGIE_MAX = 20;

        /// <summary>
        /// Vitesse de déplacement du boss
        /// </summary>
        private const int VITESSE = 3;

        /// <summary>
        /// Valeur en points du boss
        /// </summary>
        private const uint POINTS = 25;

        #endregion

        #region ATTRIBUTS

        /// <summary>
        /// Timer du boss
        /// </summary>
        private readonly Timer _bossTimer = new Timer();

        /// <summary>
        /// Laser du boss
        /// </summary>
        private Laser _laser;

        /// <summary>
        /// Énergie du boss
        /// </summary>
        private int _energie;

        /// <summary>
        /// Valeur en points du boss
        /// </summary>
        private uint _points;

        /// <summary>
        /// Indice de la direction courante
        /// </summary>
        private int _directionCourante = 4;

        /// <summary>
        /// Nombre à calculer permettant d'alterner la direction courante du boss
        /// </summary>
        private int _nbAlternantPositivite = -1;

        #endregion

        #region PROPRIÉTÉS ET INDEXEURS

        /// <summary>
        /// Obtient ou défini le laser du Boss
        /// </summary>
        public Laser Laser
        {
            get { return _laser; }
            set { _laser = value; }
        }

        /// <summary>
        /// Obtient ou défini la quantité d'énergie du boss
        /// </summary>
        public int Energie
        {
            get { return _energie; }
            set
            {
                if (value <= 0)
                {
                    value = 0;
                    Points += 1;
                }
                else if (value > Boss.ENERGIE_MAX)
                    value = Boss.ENERGIE_MAX;

                _energie = value;
            }
        }

        /// <summary>
        /// Valeur en points du boss
        /// </summary>
        public uint Points
        {
            get { return _points; }
            set
            {
                if (value > 0)
                {
                    Detruit = true;
                    _points = Boss.POINTS;
                }
            }
        }

        /// <summary>
        /// Obtient ou défini l'état détruit du boss
        /// </summary>
        public override bool Detruit
        {
            protected set
            {
                if (value)
                    _bossTimer.Stop();

                base.Detruit = value;
            }
        }

        #endregion

        #region CONSTRUCTEURS
        /// <summary>
        /// Permet de créer un ennemi final
        /// </summary>
        /// <param name="pX">Position dans l'axe des X</param>
        /// <param name="pY">Position dans l'axe des Y</param>
        public Boss(int pX, int pY) : base(pX, pY)
        {
            Energie = Boss.ENERGIE_MAX;
            Vitesse = Boss.VITESSE;
            DirectionCourante = Direction.Bas;

            Rectangle = new Rectangle(Position.X, Position.Y, Boss.LARGEUR, Boss.HAUTEUR);

            _bossTimer.Interval = 1500;
            _bossTimer.Tick += BossTimerEvent;
        }

        #endregion

        #region MÉTHODES

        /// <summary>
        /// Permet d'instancier un laser à chaque tick du timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BossTimerEvent(object sender, EventArgs e)
        {
            Laser = new Laser(Position.X + 28, Boss.HAUTEUR + 1);
        }

        /// <summary>
        /// Permet de dessiner graphiquement l'ennemi final
        /// </summary>
        /// <param name="pGraphics"></param>
        public override void Dessiner(Graphics pGraphics)
        {
            if (!VerifierVisibilite())
                Position = new Point(Position.X <= 0
                        ? 0
                        : Position.X >= Jeu.LARGEUR_ECRAN - Boss.LARGEUR
                            ? Jeu.LARGEUR_ECRAN - Rectangle.Width
                            : Position.X
                    , Position.Y > 0
                        ? 0
                        : Position.Y);

            pGraphics.DrawImage(new Bitmap(Resources.boss, Rectangle.Width, Rectangle.Height), Position);
            Rectangle = new Rectangle(Position.X, Position.Y, Rectangle.Width, Rectangle.Height);
        }

        /// <summary>
        /// Permet d'animer le boss en le déplaçant
        /// </summary>
        /// <returns>Un laser si la valeur de l'attribut n'est pas nul</returns>
        /// <remarks>
        /// On part le timer que si le boss est visible dans l'écran du jeu, puis l'on
        /// change sa direction afin qu'il se déplace de gauche à droite lorsqu'il atteint les bords.
        /// </remarks>
        public ElementGraphique Animer()
        {
            if (!_bossTimer.Enabled && Position.Y >= 0)
            {
                _bossTimer.Start();
                _directionCourante = 1;
            }

            // Alterne la direction
            if ((Position.X <= 0 || Position.X >= Jeu.LARGEUR_ECRAN - Rectangle.Width) && Position.Y >= 0)
                _directionCourante += _nbAlternantPositivite *= -1;

            Deplacer((Direction)_directionCourante);

            if (!(Laser is null))
            {
                Laser cloneDeLaser = (Laser)Laser.Clone();
                Laser = null;
                return cloneDeLaser;
            }

            return null;
        }

        #endregion
    }
}