﻿#region MÉTADONNÉES

// Nom du fichier : Missile.cs
// Auteur : Martin Vézina (MartinVézina)
// Date de création : 2021-02-05
// Date de modification : 2021-02-05

#endregion

#region USING

using _420_14C_FX_TP1.Interfaces;
using _420_14C_FX_TP1.Properties;
using System;
using System.Drawing;
using System.Windows.Forms;

#endregion

namespace _420_14C_FX_TP1.Classes
{
    /// <summary>
    /// Représente un missile
    /// </summary>
    public class Missile : ElementGraphique, ICloneable, IDommageable
    {
        #region CONSTANTES ET ATTRIBUTS STATIQUES

        /// <summary>
        /// Hauteur du missile
        /// </summary>
        private const int HAUTEUR = 10;

        /// <summary>
        /// Largeur du missile
        /// </summary>
        private const int LARGEUR = 10;

        /// <summary>
        /// Points de dommage du missile
        /// </summary>
        public const int DOMMAGE = 1;

        /// <summary>
        /// Vitesse de déplacement du missile
        /// </summary>
        private const int VITESSE = 3;

        #endregion

        #region ATTRIBUTS

        /// <summary>
        /// Timer permettant le déplacement du missile
        /// </summary>
        private readonly Timer _missileTimer = new Timer();

        /// <summary>
        /// Points de dommage du missile
        /// </summary>
        private uint _dommage;

        #endregion

        #region PROPRIÉTÉS ET INDEXEURS

        /// <summary>
        /// Obtient ou défini la quantité de dommage octroyée par le missile
        /// </summary>
        public uint Dommage
        {
            get { return _dommage; }
            set { _dommage = value; }
        }

        /// <summary>
        /// Obtient ou défini l'état détruit du missile
        /// </summary>
        public override bool Detruit
        {
            protected set
            {
                if (value)
                    _missileTimer.Stop();

                base.Detruit = value;
            }
        }
        #endregion

        #region CONSTRUCTEURS

        /// <summary>
        /// Permet la création d'un missile
        /// </summary>
        /// <param name="pX">Position dans l'axe X</param>
        /// <param name="pY">Position dans l'axe Y</param>
        /// <param name="pDirection">Direction du déplacement</param>
        /// <param name="pIntervallePourTimer">Intervalle du timer</param>
        public Missile(int pX, int pY, Direction pDirection, int pIntervallePourTimer) : base(pX, pY)
        {
            Vitesse = Missile.VITESSE;
            Dommage = Missile.DOMMAGE;
            DirectionCourante = pDirection;

            Rectangle = new Rectangle(Position.X, Position.Y, Missile.LARGEUR, Missile.HAUTEUR);

            _missileTimer.Interval = pIntervallePourTimer;

            _missileTimer.Tick += MissileTimerEvent;
        }

        #endregion

        #region MÉTHODES

        /// <summary>
        /// Événement appelé à chaque tick du timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MissileTimerEvent(object sender, EventArgs e)
        {
            Deplacer(DirectionCourante);
        }

        /// <summary>
        /// Permet de dessiner graphiquement le missile
        /// </summary>
        /// <param name="pGraphics"></param>
        public override void Dessiner(Graphics pGraphics)
        {
            if (DirectionCourante == Direction.Haut && Position.Y <= Jeu.HAUTEUR_ECRAN ||
                DirectionCourante == Direction.Bas && Position.Y + Missile.HAUTEUR >= 0)
                if (VerifierVisibilite())
                {
                    if (!_missileTimer.Enabled)
                        _missileTimer.Start();

                    pGraphics.DrawImage(new Bitmap(Resources.missile, Rectangle.Width, Rectangle.Height), Position);
                    Rectangle = new Rectangle(Position.X, Position.Y, Rectangle.Width, Rectangle.Height);
                }
                else
                    Detruit = true;
        }

        #region Overrides of ElementGraphique

        /// <summary>
        /// Détermine l'action à effectuer à la suite d'une collision avec l'élément graphique reçu en paramètre pour le missile
        /// </summary>
        /// <param name="pElementGraphique"></param>
        public override void HeurterPar(ElementGraphique pElementGraphique)
        {
            if (pElementGraphique is IPoint objPourIPoint && !(pElementGraphique is IEnergie))
                objPourIPoint.Points += 1;
            base.HeurterPar(pElementGraphique);
        }

        #endregion

        /// <summary>
        /// Permet d'endommager l'élément graphique reçu en paramètre pour le missile
        /// </summary>
        /// <param name="pElementGraphique">Élément graphique à endommager</param>
        public void Endommager(ElementGraphique pElementGraphique)
        {
            ((IEnergie)pElementGraphique).Energie -= (int)Dommage;
        }

        /// <summary>
        /// Permet au missile d'être cloné
        /// </summary>
        /// <returns>Retour le clone du missile</returns>
        public object Clone()
        {
            return new Missile(Position.X, Position.Y, DirectionCourante, _missileTimer.Interval);
        }

        #endregion

    }
}