﻿#region MÉTADONNÉES

// Nom du fichier : Direction.cs
// Auteur : Mélina Hotte (1933760)
// Date de création : 2021-02-15
// Date de modification : 2021-02-20

#endregion

namespace _420_14C_FX_TP1.Classes
{
    /// <summary>
    /// Représente une direction à prendre lors d'un déplacement
    /// </summary>
    public enum Direction
    {
        Aucune,
        Gauche,
        Droite,
        Haut,
        Bas
    }
}
