﻿#region MÉTADONNÉES

// Nom du fichier : CapsuleEnergie.cs
// Auteur : Mélina Hotte (1933760)
// Date de création : 2021-02-15
// Date de modification : 2021-02-21

#endregion

#region USING

using _420_14C_FX_TP1.Interfaces;
using _420_14C_FX_TP1.Properties;
using System.Drawing;

#endregion

namespace _420_14C_FX_TP1.Classes
{
    /// <summary>
    /// Représente une capsule d'énergie
    /// </summary>
    public class CapsuleEnergie : ElementGraphique, IAutomatisable, IEnergiser
    {
        #region CONSTANTES ET ATTRIBUTS STATIQUES

        /// <summary>
        /// Hauteur de la capsule d'énergie
        /// </summary>
        private const int HAUTEUR = 25;

        /// <summary>
        /// Largeur de la capsule d'énergie
        /// </summary>
        private const int LARGEUR = 25;

        /// <summary>
        /// Vitesse de déplacement de la capsule d'énergie
        /// </summary>
        private const int VITESSE = 2;

        /// <summary>
        /// Points d'énergie à donner de la capsule d'énergie
        /// </summary>
        private const int POINTSENERGIE = 3;

        #endregion

        #region ATTRIBUT

        /// <summary>
        /// Points d'énergie à donner de la capsule d'énergie
        /// </summary>
        private int _pointsEnergie;

        #endregion

        #region PROPRIÉTÉ ET INDEXEUR

        /// <summary>
        /// Obtient ou défini le nombre de points d'énergie de la capsule d'énergie
        /// </summary>
        public int PointsEnergie
        {
            get { return _pointsEnergie; }
            set { _pointsEnergie = value; }
        }

        #endregion

        #region CONSTRUCTEUR

        /// <summary>
        /// Permet de créer une capsule d'énergie
        /// </summary>
        /// <param name="pX">Position dans l'axe X</param>
        /// <param name="pY">Position dans l'axe Y</param>
        public CapsuleEnergie(int pX, int pY) : base(pX, pY)
        {
            PointsEnergie = CapsuleEnergie.POINTSENERGIE;
            Vitesse = CapsuleEnergie.VITESSE;
            DirectionCourante = Direction.Bas;

            Rectangle = new Rectangle(Position.X, Position.Y, CapsuleEnergie.LARGEUR, CapsuleEnergie.HAUTEUR);
        }

        #endregion

        #region MÉTHODE

        /// <summary>
        /// Permet de dessiner graphiquement la capsule d'énergie
        /// </summary>
        /// <param name="pGraphics"></param>
        public override void Dessiner(Graphics pGraphics)
        {
            if (DirectionCourante == Direction.Bas && Position.Y + CapsuleEnergie.HAUTEUR >= 0)
                if (VerifierVisibilite())
                {
                    pGraphics.DrawImage(new Bitmap(Resources.energie, Rectangle.Width, Rectangle.Height), Position);
                    Rectangle = new Rectangle(Position.X, Position.Y, Rectangle.Width, Rectangle.Height);
                }
                else
                    Detruit = true;
        }

        /// <summary>
        /// Permet d'énergiser l'élément avec lequel il entre en contact pour la capsule d'énergie
        /// </summary>
        /// <param name="pElementGraphique"></param>
        public void Energiser(ElementGraphique pElementGraphique)
        {
            ((IEnergie)pElementGraphique).Energie += PointsEnergie;
        }

        /// <summary>
        /// Permet d'animer la capsule d'énergie en la déplaçant
        /// </summary>
        /// <returns>Retourne toujours nul</returns>
        public ElementGraphique Animer()
        {
            Deplacer(DirectionCourante);
            return null;
        }

        #endregion
    }
}