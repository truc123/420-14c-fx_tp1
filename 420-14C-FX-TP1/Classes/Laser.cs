﻿#region MÉTADONNÉES

// Nom du fichier : Laser.cs
// Auteur : Mélina Hotte (1933760)
// Date de création : 2021-02-15
// Date de modification : 2021-02-21

#endregion

#region USING

using _420_14C_FX_TP1.Interfaces;
using _420_14C_FX_TP1.Properties;
using System;
using System.Drawing;
using System.Windows.Forms;

#endregion

namespace _420_14C_FX_TP1.Classes
{
    /// <summary>
    /// Représente un laser
    /// </summary>
    public class Laser : ElementGraphique, ICloneable, IDommageable
    {
        #region CONSTANTES ET ATTRIBUTS STATIQUES

        /// <summary>
        /// Hauteur du laser
        /// </summary>
        private const int HAUTEUR = 20;

        /// <summary>
        /// Largeur du laser
        /// </summary>
        private const int LARGEUR = 20;

        /// <summary>
        /// Points de dommage du laser
        /// </summary>
        public const int DOMMAGE = 2;

        /// <summary>
        /// Vitesse de déplacement du laser
        /// </summary>
        private const int VITESSE = 4;

        #endregion

        #region ATTRIBUTS

        /// <summary>
        /// Timer du laser
        /// </summary>
        private readonly Timer _laserTimer = new Timer();

        /// <summary>
        /// Points de dommage du laser
        /// </summary>
        private uint _dommage;

        #endregion

        #region PROPRIÉTÉ ET INDEXEUR

        /// <summary>
        /// Obtient ou défini l'état détruit du laser
        /// </summary>
        public override bool Detruit
        {
            protected set
            {
                if (value)
                    _laserTimer.Stop();

                base.Detruit = value;
            }
        }

        /// <summary>
        /// Obtient ou défini le nombre de points de dommage à retirer à un élément graphique possédant de l'énergie
        /// </summary>
        public uint Dommage
        {
            get { return _dommage; }
            set { _dommage = value; }
        }
        #endregion

        #region CONSTRUCTEURS

        /// <summary>
        /// Permet la création d'un laser
        /// </summary>
        /// <param name="pX">Position dans l'axe des X</param>
        /// <param name="pY">Position dans l'axe des Y</param>
        public Laser(int pX, int pY) : base(pX, pY)
        {
            Dommage = Laser.DOMMAGE;
            Vitesse = Laser.VITESSE;
            DirectionCourante = Direction.Bas;

            Rectangle = new Rectangle(Position.X, Position.Y, Laser.LARGEUR, Laser.HAUTEUR);

            _laserTimer.Interval = 1;
            _laserTimer.Tick += LaserTimerEvent;
        }

        #endregion

        #region MÉTHODES

        /// <summary>
        /// Événement appelé à chaque tick du timer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LaserTimerEvent(object sender, EventArgs e)
        {
            Deplacer(DirectionCourante);
        }

        /// <summary>
        /// Permet de dessiner graphiquement le laser
        /// </summary>
        /// <param name="pGraphics"></param>
        public override void Dessiner(Graphics pGraphics)
        {
            if (DirectionCourante == Direction.Bas && Position.Y + Laser.HAUTEUR >= 0)
                if (VerifierVisibilite())
                {
                    if (!_laserTimer.Enabled)
                        _laserTimer.Start();

                    pGraphics.DrawImage(new Bitmap(Resources.laser, Rectangle.Width, Rectangle.Height), Position);
                    Rectangle = new Rectangle(Position.X, Position.Y, Rectangle.Width, Rectangle.Height);
                }
                else
                    Detruit = true;
        }

        /// <summary>
        /// Permet d'endommager l'élément graphique reçu en paramètre pour le laser
        /// </summary>
        /// <param name="pElementGraphique"></param>
        public void Endommager(ElementGraphique pElementGraphique)
        {
            ((IEnergie)pElementGraphique).Energie -= (int)Dommage;
        }

        /// <summary>
        /// Permet de cloner le laser
        /// </summary>
        /// <returns>Clone du laser</returns>
        public object Clone()
        {
            return new Laser(Position.X, Position.Y);
        }

        #endregion
    }
}