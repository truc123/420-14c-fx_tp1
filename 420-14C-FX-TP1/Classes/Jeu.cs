﻿#region MÉTADONNÉES

// Nom du fichier : Jeu.cs
// Auteur : Mélina Hotte (1933760)
// Date de création : 2021-02-15
// Date de modification : 2021-02-24

#endregion

#region USING

using _420_14C_FX_TP1.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;

#endregion

namespace _420_14C_FX_TP1.Classes
{
    /// <summary>
    /// Classe permettant la gestion du jeu en cours
    /// </summary>
    public class Jeu
    {
        #region CONSTANTES ET ATTRIBUTS STATIQUES

        /// <summary>
        /// Largeur de l'écran du jeu
        /// </summary>
        public const int LARGEUR_ECRAN = 600;

        /// <summary>
        /// Hauteur de l'écran du jeu
        /// </summary>
        public const int HAUTEUR_ECRAN = 400;

        #endregion

        #region ATTRIBUTS

        /// <summary>
        /// Joueur
        /// </summary>
        private Joueur _joueur;

        /// <summary>
        /// Liste des éléments graphiques du jeu
        /// </summary>
        private List<ElementGraphique> _lstElementsGraphiques;

        #endregion

        #region PROPRIÉTÉS ET INDEXEURS

        /// <summary>
        /// Obtient ou défini le joueur
        /// </summary>
        /// <exception cref="ArgumentNullException">Lancée lorsque le joueur est nul</exception>
        public Joueur Joueur
        {
            get { return _joueur; }
            set
            {
                if (value is null)
                    throw new ArgumentNullException("Joueur", "Le joueur ne peut pas être nul.");

                _joueur = value;
            }
        }

        /// <summary>
        /// Obtient ou défini la liste des éléments graphiques
        /// </summary>
        public List<ElementGraphique> LstElementsGraphiques
        {
            get { return _lstElementsGraphiques; }
            set
            {
                if (value is null)
                    throw new ArgumentNullException("LstElementsGraphiques", "La liste des éléments graphiques ne peut pas être nulle.");
                _lstElementsGraphiques = value;
            }
        }

        #endregion

        #region CONSTRUCTEURS

        /// <summary>
        /// Constructeur paramétré permettant la gestion d'une partie de jeu
        /// </summary>
        /// <param name="pJoueur">Joueur du jeu en cours</param>
        public Jeu(Joueur pJoueur)
        {
            if (!(pJoueur is null))
                Joueur = pJoueur;
            LstElementsGraphiques = new List<ElementGraphique>();
        }

        #endregion

        #region MÉTHODES

        /// <summary>
        /// Permet de commencer une partie
        /// </summary>
        /// <remarks>Le vaisseau du joueur et tous les éléments du jeu sont créés et ajoutés à la liste des éléments graphiques.</remarks>
        public void DemarrerPartie()
        {
            try
            {
                LstElementsGraphiques.Add(Joueur.Vaisseau);
                LstElementsGraphiques.Add(new Asteroide(557, -128));
                LstElementsGraphiques.Add(new Ennemi(400, -268));
                LstElementsGraphiques.Add(new CapsuleEnergie(376, -312));
                LstElementsGraphiques.Add(new Ennemi(58, -328));
                LstElementsGraphiques.Add(new Boss(Jeu.LARGEUR_ECRAN / 2 - 38, -1000));
            }
            catch (Exception msgErreur)
            {
                MessageBox.Show(
                    $"Une erreur s'est produite : {msgErreur}\n\nVeuillez communiquer avec l'administrateur du système.",
                    "Jeu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ErreurLog.Journaliser($"DemarrerPartie : {msgErreur}", TraceEventType.Critical, 2);
            }
        }

        /// <summary>
        /// Permet de trouver les collisions entre les différents éléments du jeu
        /// </summary>
        /// <remarks>
        /// Suite à une collision, si un objet est détruit, il est retiré de la liste des éléments graphiques du jeu.
        /// De plus, si un élément graphique détruit possède des points, ceux-ci sont ajoutés au pointage du joueur.
        /// </remarks>
        public void ValiderCollisions()
        {
            // Vérification des collisions
            for (int i = 0; i < LstElementsGraphiques.Count; i++)
            {
                // L'on veut éviter que i == j et on ne veut pas que i ait déjà été comparé avec j. Alors on fait i + 1.
                for (int j = i + 1; j < LstElementsGraphiques.Count; j++)
                {
                    if (LstElementsGraphiques[i].Detruit || LstElementsGraphiques[j].Detruit) continue;
                    if (!LstElementsGraphiques[i].Rectangle.IntersectsWith(LstElementsGraphiques[j].Rectangle)) continue;

                    LstElementsGraphiques[i].HeurterPar(LstElementsGraphiques[j]);

                    if (LstElementsGraphiques[j].Detruit)
                    {
                        IPoint objIPoint = null;
                        if (LstElementsGraphiques[j] is IPoint objDeJ)
                            objIPoint = objDeJ;
                        else if (LstElementsGraphiques[i] is IPoint objDeI)
                            objIPoint = objDeI;

                        if (!(objIPoint is null) && objIPoint.Points > 0)
                            Joueur.Pointage += objIPoint.Points;
                    }
                }
            }

            // Vérification des éléments détruits à enlever de la liste
            if ((Joueur.Vaisseau.Detruit || !LstElementsGraphiques.Exists(x => x is Boss)) && LstElementsGraphiques.Count > 0)
            {
                for (int i = Joueur.Vaisseau.Detruit ? 0 : 1; i < LstElementsGraphiques.Count; i++)
                    if (!LstElementsGraphiques[i].Detruit)
                        LstElementsGraphiques[i].HeurterPar(new Asteroide(
                            LstElementsGraphiques[i].Position.X,
                            LstElementsGraphiques[i].Position.Y));
            }

            //Retrait des éléments graphiques détruits de la liste des éléments graphiques.
            LstElementsGraphiques.RemoveAll(x => x.Detruit);
        }

        /// <summary>
        /// Permet l'animation des éléments graphiques du jeu qui peuvent être animés de manière automatique
        /// </summary>
        /// <remarks>
        /// Suite à l'animation d'un élément du jeu, il se peut que le résultat de cette animation soit la création d'un nouvel élément.
        /// Celui-ci doit donc être ajouté à la liste des éléments graphiques du jeu.
        /// </remarks>
        public void AnimerBots()
        {
            for (int i = 0; i < LstElementsGraphiques.Count; i++)
            {
                if (!(LstElementsGraphiques[i] is IAutomatisable obj)) continue;
                ElementGraphique cloneDeProjectile = obj.Animer();

                //Vérifie si la méthode a retourné un élément graphique
                if (!(cloneDeProjectile is null))
                    LstElementsGraphiques.Add(cloneDeProjectile);
            }
        }

        #endregion
    }
}