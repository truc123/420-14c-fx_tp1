﻿#region MÉTADONNÉES

// Nom du fichier : DatabaseException.cs
// Auteur : Mélina Hotte (1933760)
// Date de création : 2021-02-15
// Date de modification : 2021-02-24

#endregion

#region USING

using System;
using System.Diagnostics;

#endregion

namespace _420_14C_FX_TP1.Classes
{
    /// <summary>
    /// Classe représentant une exception personnalisée lorsqu'il y a une erreur en lien avec une base de donnée
    /// </summary>
    public class DatabaseException : Exception
    {
        #region CONSTRUCTEUR

        /// <summary>
        /// Permet de créer une exception DatabaseException
        /// </summary>
        /// <param name="pErreurMsg">Message de l'erreur lancée</param>
        public DatabaseException(string pErreurMsg) : base(
            "La base de données est présentement inaccessible. Ainsi, vous ne pourrez pas enregistrer votre résultat.")
        {
            ErreurLog.Journaliser(pErreurMsg, TraceEventType.Warning, 1);
        }

        #endregion
    }
}