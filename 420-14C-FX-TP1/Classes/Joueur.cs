﻿#region MÉTADONNÉES

// Nom du fichier : Joueur.cs
// Auteur : Mélina Hotte (1933760)
// Date de création : 2021-02-15
// Date de modification : 2021-02-26

#endregion

#region USING

using System;

#endregion

namespace _420_14C_FX_TP1.Classes
{
    /// <summary>
    /// Représente un joueur
    /// </summary>
    public class Joueur
    {
        #region CONSTANTES ET ATTRIBUTS STATIQUES

        /// <summary>
        /// Nombre de caractères minimum pour le nom d'un joueur
        /// </summary>
        public const int NOM_NB_CAR_MIN = 3;

        /// <summary>
        /// Nombre de caractères maximales pour le nom d'un joueur
        /// </summary>
        public const int NOM_NB_CAR_MAX = 20;

        #endregion

        #region ATTRIBUTS

        /// <summary>
        /// Nom du joueur
        /// </summary>
        private string _nom;

        /// <summary>
        /// Nombre de points accumulé par le joueur lors d'une partie
        /// </summary>
        private uint _pointage;

        /// <summary>
        /// Date d'enregistrement du résultat du pointage
        /// </summary>
        private DateTime _dateResultat;

        /// <summary>
        /// Vaisseau du joueur
        /// </summary>
        private VaisseauJoueur _vaisseau;

        #endregion

        #region PROPRIÉTÉS ET INDEXEURS

        /// <summary>
        /// Obtient ou défini le nom du joueur
        /// </summary>
        /// <exception cref="ArgumentNullException">Lancée lorsque le nom du joueur est vide ou nul</exception>
        /// <exception cref="ArgumentOutOfRangeException">Lancée lorsque le nombre de caractère est plus grand que NOM_CARC_MIN ou plus grand que NOM_CARC_MAX</exception>
        public string Nom
        {
            get { return _nom; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentNullException("_nom", "Le nom ne peut être nul ou vide.");

                int nbCharPourValeurNom = value.Trim().Length;

                if (nbCharPourValeurNom < 5 && nbCharPourValeurNom > 20)
                    throw new ArgumentNullException("_nom", "Le nom ne peut être nul ou vide.");

                _nom = value;
            }
        }

        /// <summary>
        /// Obtient ou défini le pointage du joueur
        /// </summary>
        public uint Pointage
        {
            get { return _pointage; }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("_pointage", value, "La valeur ne peut être inférieure à 0.");
                _pointage = value;
            }
        }

        /// <summary>
        /// Obtient ou défini la date d'enregistrement du résultat du joueur
        /// </summary>
        public DateTime DateResultat
        {
            get { return _dateResultat; }
            set { _dateResultat = value; }
        }

        /// <summary>
        /// Obtient ou défini le vaisseau utilisé par le joueur
        /// </summary>
        public VaisseauJoueur Vaisseau
        {
            get { return _vaisseau; }
            set { _vaisseau = value; }
        }

        #endregion

        #region CONSTRUCTEURS

        /// <summary>
        /// Constructeur paramétré permettant de créer un joueur
        /// </summary>
        /// <param name="pNom">Nom du joeur</param>
        public Joueur(string pNom)
        {
            Nom = pNom;
            Vaisseau = new VaisseauJoueur(Jeu.LARGEUR_ECRAN / 2 - 25, Jeu.HAUTEUR_ECRAN - 50);
        }

        /// <summary>
        /// Constructeur paramétré permettant de créer un joueur
        /// </summary>
        /// <param name="nom">Nom du joueur</param>
        /// <param name="pointage">Pointage du joueur</param>
        /// <param name="dateResultat">Date d'enregistrement du résultat du joueur</param>
        public Joueur(string pNom, uint pPointage, DateTime pDateResultat) : this(pNom)
        {
            Pointage = pPointage;
            DateResultat = pDateResultat;
        }

        #endregion

        #region MÉTHODES

        #region MÉTHODE

        /// <summary>
        /// Permet le représentation du joueur sous forme de chaîne de caractères
        /// </summary>
        /// <returns>Chaîne de caractère représentant le joueur.</returns>
        public override string ToString()
        {
            return $"{DateResultat.ToShortDateString()}  {Nom,-20} {Pointage}";
        }

        #endregion

        #endregion
    }
}