﻿#region MÉTADONNÉES

// Nom du fichier : Ennemi.cs
// Auteur : Mélina Hotte (1933760)
// Date de création : 2021-02-15
// Date de modification : 2021-02-26

#endregion

#region USING

using _420_14C_FX_TP1.Interfaces;
using _420_14C_FX_TP1.Properties;
using System;
using System.Drawing;
using System.Windows.Forms;

#endregion

namespace _420_14C_FX_TP1.Classes
{
    /// <summary>
    /// Représente un ennemi de base
    /// </summary>
    public class Ennemi : ElementGraphique, IAutomatisable, IDommageable, IPoint
    {
        #region CONSTANTES ET ATTRIBUTS STATIQUES

        /// <summary>
        /// Hauteur de l'ennemi
        /// </summary>
        private const int HAUTEUR = 30;

        /// <summary>
        /// Largeur de l'ennemi
        /// </summary>
        private const int LARGEUR = 30;

        /// <summary>
        /// Vitesse de déplacement de l'ennemi
        /// </summary>
        private const int VITESSE = 2;

        /// <summary>
        /// Valeur en points de l'ennemi
        /// </summary>
        private const uint POINTS = 5;

        /// <summary>
        /// Points de dommage de l'ennemi
        /// </summary>
        private const uint DOMMAGE = 3;

        #endregion

        #region ATTRIBUTS

        /// <summary>
        /// Timer de l'ennemi
        /// </summary>
        private readonly Timer _ennemiTimer = new Timer();

        /// <summary>
        /// Missile de l'ennemi
        /// </summary>
        private Missile _missile;

        /// <summary>
        /// Nombre de points de dommage à retirer à un élément graphique possédant de l'énergie
        /// </summary>
        private uint _dommage;

        /// <summary>
        /// Valeurs en points de l'ennemi
        /// </summary>
        private uint _points;

        #endregion

        #region PROPRIÉTÉS ET INDEXEURS

        /// <summary>
        /// Obtient ou défini le missile de l'ennemi
        /// </summary>
        public Missile Missile
        {
            get { return _missile; }
            set { _missile = value; }
        }

        /// <summary>
        /// Obtient ou défini l'état détruit de l'ennemi
        /// </summary>
        public override bool Detruit
        {
            protected set
            {
                if (value)
                    _ennemiTimer.Stop();

                base.Detruit = value;
            }
        }

        /// <summary>
        /// Obtient ou défini le nombre de points d'énergie à diminuer
        /// </summary>
        public uint Dommage
        {
            get { return _dommage; }
            set { _dommage = value; }
        }

        /// <summary>
        /// Valeur en points de l'ennemi
        /// </summary>
        public uint Points
        {
            get { return _points; }
            set
            {
                if (value > 0)
                {
                    _points = Ennemi.POINTS;
                    Detruit = true;
                }
            }
        }
        #endregion

        #region CONSTRUCTEUR

        /// <summary>
        /// Permet de créer un ennemi
        /// </summary>
        /// <param name="pX">Position dans l'axe des X</param>
        /// <param name="pY">Position dans l'axe des Y</param>
        public Ennemi(int pX, int pY) : base(pX, pY)
        {
            DirectionCourante = Direction.Bas;
            Vitesse = Ennemi.VITESSE;
            Dommage = Ennemi.DOMMAGE;
            Rectangle = new Rectangle(Position.X, Position.Y, Ennemi.LARGEUR, Ennemi.HAUTEUR);

            _ennemiTimer.Interval = 1000;
            _ennemiTimer.Tick += EnnemiTimerEvent;
        }

        #endregion

        #region MÉTHODES

        /// <summary>
        /// Permet d'instancier un missile à chaque tick du timer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EnnemiTimerEvent(object sender, EventArgs e)
        {
            Missile = new Missile(Position.X + 10, Position.Y + Ennemi.HAUTEUR + 5,
                DirectionCourante, 1);
        }

        /// <summary>
        /// Permet de dessiner graphiquement l'ennemi
        /// </summary>
        /// <param name="pGraphics"></param>
        public override void Dessiner(Graphics pGraphics)
        {
            if (DirectionCourante == Direction.Bas && Position.Y + Ennemi.HAUTEUR >= 0)
                if (VerifierVisibilite() && !Detruit)
                {
                    pGraphics.DrawImage(new Bitmap(Resources.ennemi, Rectangle.Width, Rectangle.Height), Position);
                    Rectangle = new Rectangle(Position.X, Position.Y, Rectangle.Width, Rectangle.Height);
                }
                else
                    Detruit = true;
        }

        /// <summary>
        /// Détermine l'action à effectuer suite à une collision avec l'élément graphique reçu en paramètre pour l'ennemi
        /// </summary>
        /// <param name="pElementGraphique"></param>
        public override void HeurterPar(ElementGraphique pElementGraphique)
        {
            base.HeurterPar(pElementGraphique);

            if (pElementGraphique is VaisseauJoueur || pElementGraphique is Missile { DirectionCourante: Direction.Haut })
                Points += 1;
        }

        /// <summary>
        /// Permet d'animer l'ennemi en le déplaçant et de commencer à tirer des missiles lorsqu'il est visible dans l'écran du jeu
        /// </summary>
        /// <returns>Un missile si la valeur de l'attribut n'est pas nul</returns>
        public ElementGraphique Animer()
        {
            if (!_ennemiTimer.Enabled && Position.Y >= 0)
                _ennemiTimer.Start();

            Deplacer(DirectionCourante);

            if (!(Missile is null))
            {
                Missile cloneDeMissile = (Missile)Missile.Clone();
                Missile = null;
                return cloneDeMissile;
            }

            return null;
        }

        /// <summary>
        /// Permet d'endommager l'élément graphique reçu en paramètre pour l'ennemi
        /// </summary>
        /// <param name="pElementGraphique"></param>
        public void Endommager(ElementGraphique pElementGraphique)
        {
            ((IEnergie)pElementGraphique).Energie -= (int)Dommage;
        }

        #endregion
    }
}