﻿namespace _420_14C_FX_TP1
{
    partial class FrmJeu
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tmrJeu = new System.Windows.Forms.Timer(this.components);
            this.pnlEcran = new System.Windows.Forms.PictureBox();
            this.lblPoints = new System.Windows.Forms.Label();
            this.lblPointage = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pgbEnergie = new System.Windows.Forms.ProgressBar();
            this.gbClassement = new System.Windows.Forms.GroupBox();
            this.lstClassement = new System.Windows.Forms.ListBox();
            this.btnNouvellePartie = new System.Windows.Forms.Button();
            this.btnQuitter = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pnlEcran)).BeginInit();
            this.gbClassement.SuspendLayout();
            this.SuspendLayout();
            // 
            // tmrJeu
            // 
            this.tmrJeu.Interval = 1;
            this.tmrJeu.Tick += new System.EventHandler(this.TrmJeu_Tick);
            // 
            // pnlEcran
            // 
            this.pnlEcran.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.pnlEcran.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlEcran.Location = new System.Drawing.Point(13, 44);
            this.pnlEcran.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pnlEcran.Name = "pnlEcran";
            this.pnlEcran.Size = new System.Drawing.Size(600, 488);
            this.pnlEcran.TabIndex = 0;
            this.pnlEcran.TabStop = false;
            this.pnlEcran.Paint += new System.Windows.Forms.PaintEventHandler(this.PnlEcran_Paint);
            // 
            // lblPoints
            // 
            this.lblPoints.AutoSize = true;
            this.lblPoints.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPoints.Location = new System.Drawing.Point(9, 11);
            this.lblPoints.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPoints.Name = "lblPoints";
            this.lblPoints.Size = new System.Drawing.Size(69, 23);
            this.lblPoints.TabIndex = 1;
            this.lblPoints.Text = "Points :";
            // 
            // lblPointage
            // 
            this.lblPointage.AutoSize = true;
            this.lblPointage.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPointage.Location = new System.Drawing.Point(73, 11);
            this.lblPointage.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPointage.Name = "lblPointage";
            this.lblPointage.Size = new System.Drawing.Size(104, 23);
            this.lblPointage.TabIndex = 2;
            this.lblPointage.Text = "{lblPointage}";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(324, 9);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 23);
            this.label3.TabIndex = 3;
            this.label3.Text = "Énergie :";
            // 
            // pgbEnergie
            // 
            this.pgbEnergie.Location = new System.Drawing.Point(405, 15);
            this.pgbEnergie.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pgbEnergie.Maximum = 10;
            this.pgbEnergie.Name = "pgbEnergie";
            this.pgbEnergie.Size = new System.Drawing.Size(197, 19);
            this.pgbEnergie.TabIndex = 5;
            this.pgbEnergie.Value = 9;
            // 
            // gbClassement
            // 
            this.gbClassement.Controls.Add(this.lstClassement);
            this.gbClassement.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbClassement.Location = new System.Drawing.Point(623, 35);
            this.gbClassement.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbClassement.Name = "gbClassement";
            this.gbClassement.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbClassement.Size = new System.Drawing.Size(323, 444);
            this.gbClassement.TabIndex = 8;
            this.gbClassement.TabStop = false;
            this.gbClassement.Text = "Classement";
            // 
            // lstClassement
            // 
            this.lstClassement.Font = new System.Drawing.Font("Comic Sans MS", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstClassement.FormattingEnabled = true;
            this.lstClassement.ItemHeight = 14;
            this.lstClassement.Location = new System.Drawing.Point(12, 35);
            this.lstClassement.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lstClassement.Name = "lstClassement";
            this.lstClassement.Size = new System.Drawing.Size(296, 382);
            this.lstClassement.TabIndex = 0;
            // 
            // btnNouvellePartie
            // 
            this.btnNouvellePartie.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNouvellePartie.Location = new System.Drawing.Point(623, 495);
            this.btnNouvellePartie.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnNouvellePartie.Name = "btnNouvellePartie";
            this.btnNouvellePartie.Size = new System.Drawing.Size(153, 37);
            this.btnNouvellePartie.TabIndex = 99;
            this.btnNouvellePartie.TabStop = false;
            this.btnNouvellePartie.Text = "Nouvelle Partie";
            this.btnNouvellePartie.UseVisualStyleBackColor = true;
            this.btnNouvellePartie.Click += new System.EventHandler(this.BtnNouvellePartie_Click);
            // 
            // btnQuitter
            // 
            this.btnQuitter.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuitter.Location = new System.Drawing.Point(788, 495);
            this.btnQuitter.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnQuitter.Name = "btnQuitter";
            this.btnQuitter.Size = new System.Drawing.Size(158, 37);
            this.btnQuitter.TabIndex = 100;
            this.btnQuitter.TabStop = false;
            this.btnQuitter.Text = "Quitter";
            this.btnQuitter.UseVisualStyleBackColor = true;
            this.btnQuitter.Click += new System.EventHandler(this.BtnQuitter_Click);
            // 
            // frmJeu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(956, 541);
            this.Controls.Add(this.btnQuitter);
            this.Controls.Add(this.btnNouvellePartie);
            this.Controls.Add(this.gbClassement);
            this.Controls.Add(this.pgbEnergie);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblPointage);
            this.Controls.Add(this.lblPoints);
            this.Controls.Add(this.pnlEcran);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FrmJeu";
            this.Text = "Galactica";
            this.Load += new System.EventHandler(this.FrmJeu_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmJeu_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FrmJeu_KeyPress);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FrmJeu_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.pnlEcran)).EndInit();
            this.gbClassement.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer tmrJeu;
        private System.Windows.Forms.PictureBox pnlEcran;
        private System.Windows.Forms.Label lblPoints;
        private System.Windows.Forms.Label lblPointage;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ProgressBar pgbEnergie;
        private System.Windows.Forms.GroupBox gbClassement;
        private System.Windows.Forms.ListBox lstClassement;
        private System.Windows.Forms.Button btnNouvellePartie;
        private System.Windows.Forms.Button btnQuitter;
    }
}

